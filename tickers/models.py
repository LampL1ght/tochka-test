from django.db import models
from django.db.utils import IntegrityError
from django.utils.text import slugify
from django.utils.translation import gettext as _

from tickers.managers import PeriodHistoryManager


class Stock(models.Model):
    class Meta:
        verbose_name = _('Stock')
        verbose_name_plural = _("Stocks")

    name = models.SlugField(max_length=7, primary_key=True, verbose_name=_('Stock symbols'))

    def __str__(self):
        return self.name


class StockHistory(models.Model):
    class Meta:
        verbose_name = _('Stock trade history')
        verbose_name_plural = _("Stocks trade history")
        ordering = ['trade_date']
        unique_together = ('trade_date', 'stock')

    objects = PeriodHistoryManager()

    stock = models.ForeignKey(
        'tickers.Stock', related_name='trade_history',
        verbose_name=_('Stock'), on_delete=models.CASCADE
    )

    trade_date = models.DateField(verbose_name=_('Trade date'), db_index=True)
    open_price = models.DecimalField(max_digits=20, decimal_places=2, verbose_name=_('Open price'))
    high_price = models.DecimalField(max_digits=20, decimal_places=2, verbose_name=_('Max price'))
    low_price = models.DecimalField(max_digits=20, decimal_places=2, verbose_name=_('Min price'))
    last_price = models.DecimalField(max_digits=20, decimal_places=2, verbose_name=_('Last price'))
    volume = models.IntegerField(verbose_name=_('Trade value'))


class EmployeeRelation(models.Model):
    name = models.CharField(max_length=30, primary_key=True)


class Employee(models.Model):
    class Meta:
        unique_together = ('full_name', 'relation')

    full_name = models.CharField(max_length=50)
    slug_name = models.SlugField(max_length=50)
    relation = models.ForeignKey(
        'tickers.EmployeeRelation', on_delete=models.CASCADE
    )

    def save(self, *args, **kwargs):
        orig_slug = slugify(self.full_name)
        self.slug_name = orig_slug
        numb = 1
        while True:
            try:
                return super().save(*args, **kwargs)
            except IntegrityError:
                self.slug_name = f'{orig_slug}-{numb}'
                numb += 1


class TransactionType(models.Model):
    name = models.CharField(max_length=50, primary_key=True)


class StockInnerTradesHistory(models.Model):
    class Meta:
        verbose_name = _("Inner stock trade history")
        verbose_name_plural = _("Inner stocks trades history")

    stock = models.ForeignKey(
        'tickers.Stock', related_name='inner_history',
        verbose_name=_("Stock"), on_delete=models.CASCADE
    )
    insider = models.ForeignKey(
        'tickers.Employee', related_name='inner_history',
        verbose_name=_('Inner trades'), on_delete=models.DO_NOTHING
    )
    transaction_type = models.ForeignKey(
        'tickers.TransactionType', related_name='inner_history',
        verbose_name=_('Transaction type'), on_delete=models.DO_NOTHING
    )

    trade_date = models.DateField(verbose_name=_('Trade date'))
    owner_type = models.CharField(max_length=10, verbose_name=_('Owner type'))
    shares_traded = models.IntegerField(verbose_name=_('Shares traded'))
    last_price = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    shares_held = models.IntegerField(verbose_name=_('Shares traded'))

    def __str__(self):
        return f"{self.trade_date}: {self.stock}"
