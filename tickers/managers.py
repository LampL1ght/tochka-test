from django.db import models
from tickers.exceptions import NumberFound
from datetime import datetime, timedelta


class TradeHistoryQueryset(models.QuerySet):
    def three_months(self):
        three_month = datetime.today() - timedelta(days=90)
        return self.filter(trade_date__gte=three_month)

    def delta_sort_by_value(self, lookup_value, field_prefix):
        output = list()

        if lookup_value and field_prefix:
            for index, obj in enumerate(self):
                last_price = getattr(obj, f'{field_prefix}_price')

                #  try/except блок для прекращения вложенной итерации
                try:
                    #  итерируем только с места текущей проверяемой цены. Нет смысла проверять предыдущие значения
                    for second_obj in self[index:]:

                        current_price = getattr(second_obj, f'{field_prefix}_price')
                        price_change = current_price - last_price

                        # найдено ли нужное нам значение в результате вычетания
                        if price_change >= lookup_value:
                            # если найдено, то добавляем в массив объект для получения данных
                            output.append(
                                {
                                    "obj_from": obj,
                                    "obj_to": second_obj,
                                    "price_change": price_change
                                }
                            )
                            raise NumberFound()

                except NumberFound:
                    continue

        return output

class PeriodHistoryManager(models.Manager):
    def three_months(self):
        return self.get_queryset().three_months()

    def delta_sort_by_value(self, value, field_prefix):
        return self.get_queryset().delta_sort_by_value(value, field_prefix)

    def get_queryset(self):
        return TradeHistoryQueryset(self.model, using=self._db)
