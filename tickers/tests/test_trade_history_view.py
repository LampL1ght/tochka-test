from decimal import Decimal
from datetime import timedelta, date
from django.test import TestCase
from django.urls import reverse
from tickers.models import StockHistory, Stock


class StockViewsTest(TestCase):
    def setUp(self):
        pass

    fixtures = [
        'tickers/fixtures/stocks.json',
        'tickers/fixtures/stocks-history.json',
    ]

    def test_trade_history_3_months_aapl(self):
        url = reverse('trade-history', kwargs={"stock": "aapl"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('tickers/stockhistory_list.html')

    def test_api_trade_history_3_months_aapl(self):
        url = reverse('api-trade-history', kwargs={"stock": "aapl"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        first_obj = response.json()[0]
        date_three_months_ago = date.today() - timedelta(days=90)
        api_day, api_month, api_year = first_obj.get('trade_date').split('/')
        self.assertEqual(
            date(year=int(api_year), month=int(api_month), day=int(api_day)) >= date_three_months_ago, True
        )

        last_obj = response.json()[-1]
        api_day, api_month, api_year = last_obj.get('trade_date').split('/')
        # Чтобы тесты не зафейлились даже в выходные
        date_three_days_ago = date.today() - timedelta(days=3)
        self.assertEqual(
            date(year=int(api_year), month=int(api_month), day=int(api_day)) <= date_three_days_ago, True
        )

    def test_trade_history_analytics_empty_queryset(self):
        url = reverse('trade-history-analytics', kwargs={"stock": "aapl"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('tickers/trade_history_analytics.html')

    def test_api_trade_history_analytics_empty_queryset(self):
        url = reverse('api-analytics-trade-history', kwargs={"stock": "aapl"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), StockHistory.objects.filter(stock='aapl').count())
        first_obj = response.json()[0]

        self.assertEqual(
            Decimal(first_obj.get('high_price')) - Decimal(first_obj.get('low_price')),
            Decimal(str(first_obj.get('delta_high_low')))
        )
        self.assertEqual(
            Decimal(first_obj.get('last_price')) - Decimal(first_obj.get('open_price')),
            Decimal(str(first_obj.get('delta_close_open')))
        )

    def test_api_trade_history_analytics_get_params_week_ago(self):
        url = reverse('api-analytics-trade-history', kwargs={"stock": "aapl"})
        period_start = date.today() - timedelta(days=8)
        period_end = date.today() - timedelta(days=1)

        data = {
            "date_from": period_start.strftime('%d/%m/%Y'),
            "date_to": period_end.strftime('%d/%m/%Y')
        }

        response = self.client.get(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            len(response.json()),
            StockHistory.objects.filter(stock='aapl', trade_date__range=(period_start, period_end)).count()
        )
        first_obj = response.json()[0]

        self.assertEqual(
            Decimal(first_obj.get('high_price')) - Decimal(first_obj.get('low_price')),
            Decimal(str(first_obj.get('delta_high_low')))
        )
        self.assertEqual(
            Decimal(first_obj.get('last_price')) - Decimal(first_obj.get('open_price')),
            Decimal(str(first_obj.get('delta_close_open')))
        )

    def test_prices_delta_success(self):
        url = reverse('trade-history-delta', kwargs={"stock": "aapl"})

        data = {
            "value": 11,
            "type": "open"
        }

        response = self.client.get(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('tickers/trade_history_delta.html')

    def test_api_prices_delta_success(self):
        """
        Вот в этом местечке в задании была ошибка небольшая. Если число за 01.01 равно x+2, то получается,
        что на 08.01 мы получаем x+11 а (x+11) - (x+2) != 11. А вот если считать с предыдущего числа, то все сходится.
        В теории это могло бы работать если бы задание звучало слегка подругому и для просчетов можно было использовать
        цену открытия. А по факту получается, что сортировать нужно по фактическим числам за день.
        """
        custom_stock_name = 'stk'
        stock = Stock.objects.create(name=custom_stock_name)
        starting_object = StockHistory.objects.create(
            stock=stock,
            open_price=10,
            last_price='0',
            high_price='0',
            low_price='0',
            volume=4815162,
            trade_date=date(year=2017, month=12, day=31)
        )
        prev_price = starting_object.open_price
        bulk_create = []
        for i in range(1, 10):
            obj = StockHistory(
                stock=stock,
                open_price=prev_price,
                last_price='0',
                high_price='0',
                low_price='0',
                volume=4815162,
                trade_date=date(year=2018, month=1, day=i)
            )

            if obj.trade_date.day in [1, 2, 3, 4, 5]:
                obj.open_price += 2
            elif obj.trade_date.day in [6, 7]:
                obj.open_price -= 1
            elif obj.trade_date.day in [8]:
                obj.open_price += 3
            elif obj.trade_date.day in [9]:
                obj.open_price -= 5

            prev_price = obj.open_price
            bulk_create.append(obj)

        StockHistory.objects.bulk_create(bulk_create)

        url = reverse('api-delta-trade-history', kwargs={"stock": custom_stock_name})

        data = {
            "value": 11,
            "type": "open"
        }

        self.assertEqual(StockHistory.objects.filter(stock=stock).count(), 10)

        response = self.client.get(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)
        for data in response.json():

            first_obj = data.get('obj_from')
            last_obj = data.get('obj_to')

            self.assertEqual(
                first_obj.get('trade_date'),
                starting_object.trade_date.strftime("%d/%m/%Y")
            )
            self.assertEqual(
                last_obj.get('trade_date'),
                StockHistory.objects.get(
                    stock=stock, trade_date=date(year=2018, month=1, day=8)
                ).trade_date.strftime("%d/%m/%Y")
            )

