from django.test import TestCase
from django.urls import reverse


class StockViewsTest(TestCase):
    def setUp(self):
        pass

    fixtures = ['tickers/fixtures/stocks.json']

    def test_stocks_list_view(self):
        url = reverse('stocks-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('tickers/stock_list.html')

    def test_stocks_list_api_view(self):
        url = reverse('api-stocks-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 7)
