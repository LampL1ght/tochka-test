from datetime import datetime, timedelta
from django.test import TestCase
from django.core.management import call_command
from tickers.parser.main import Parser
from tickers.models import StockHistory, Stock, StockInnerTradesHistory, EmployeeRelation, Employee


class TestParserTradeHistory(TestCase):
    def test_parser_trade_history(self):
        call_command('parse_tickers', 1, Parser.PARSE_HISTORICAL_DATA)
        stocks_values = open('./tickers.txt', 'r').readlines()
        self.assertEqual(len(stocks_values), Stock.objects.count())

        random_stock = Stock.objects.get(name__iexact=stocks_values[0].strip())

        # Проверяем так, чтобы даже запустив тесты в выходные ошибочки не появлялись
        three_days_ago = datetime.today() - timedelta(days=3)
        self.assertEqual(
            StockHistory.objects.filter(
                trade_date__gte=three_days_ago, stock=random_stock
            ).exists(), True
        )

        three_months_ago = datetime.today() - timedelta(days=90)
        self.assertEqual(
            StockHistory.objects.filter(
                trade_date__lte=three_months_ago, stock=random_stock
            ).exists(), True
        )

        trading_weeks = 90 / 7
        weekends = 2
        number_of_trading_days = 90 - (weekends * trading_weeks)
        # Кол-во записей примерно равно кол-ву рабочих дней
        self.assertGreaterEqual(
            random_stock.trade_history.count(), int(number_of_trading_days)
          )

    def test_parser_inner_trades(self):
        """
        На макоси постоянно фейлится и так и не смог в нормальном виде запустить
        """
        # call_command('parse_tickers', 1, Parser.PARSE_INNER_TRADES)
