from django.test import TestCase
from django.urls import reverse
from tickers.models import StockInnerTradesHistory, Employee


class StockViewsTest(TestCase):
    def setUp(self):
        pass

    fixtures = [
        'tickers/fixtures/stocks.json',
        'tickers/fixtures/stocks-history.json',
        'tickers/fixtures/inside-trades.json',
    ]

    def test_insiders_list_view(self):
        url = reverse('trade-history-insiders', kwargs={"stock": "aapl"})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('tickers/stock_inner_trade_history_list.html')

    def test_api_insiders_list_view(self):
        url = reverse('api-trade-history-insiders', kwargs={"stock": "aapl"})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            len(response.json()),
            StockInnerTradesHistory.objects.filter(stock="aapl").count()
        )

    def test_insider_user_details(self):
        user = Employee.objects.first()
        url = reverse(
            'employee-trade-history',
            kwargs={"stock": "aapl", "employee": user.slug_name}
        )
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('tickers/inner_employee_trade_history_list.html')

    def test_api_insider_user_details(self):
        user = StockInnerTradesHistory.objects.filter(stock='aapl').first().insider
        url = reverse(
            'api-employee-trade-history',
            kwargs={"stock": "aapl", "employee": user.slug_name}
        )
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), user.inner_history.count())
