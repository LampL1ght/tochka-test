from datetime import datetime
from django.core.exceptions import ImproperlyConfigured


class FilterDate:
    date_mask = "%d/%m/%Y"
    filter_model_date_by = None

    def _convert_dates(self, date):
        """
        :param date: str date with mask as self.date_mask which is strftime for date
        :return: datetime obj
        """
        return datetime.strptime(date, self.date_mask)

    def get_filter_dates(self):
        """
        :return: if request.GET contains date_from and date_to returns tuple of dates
        """
        date_from = self.request.GET.get('date_from', False)
        date_to = self.request.GET.get('date_to', False)
        if date_from and date_to:
            return date_from, date_to
        else:
            return None

    def by_date_filtered_queryset(self, queryset,  dates=None):
        """
        :param queryset: django orm queryset
        :param dates: tuple of (date_from, date_to)
        :return: filtered by Date django orm queryset
        """
        if dates and self.filter_model_date_by:
            date_from, date_to = dates

            date_filter = dict()
            date_filter[f"{self.filter_model_date_by}__range"] = (
                self._convert_dates(date_from), self._convert_dates(date_to)
            )

            return queryset.filter(
                **date_filter
            )
        else:
            if not self.filter_model_date_by:
                raise ImproperlyConfigured('Your class is missing filter_by_date attribute')
            return queryset
