from decimal import Decimal
from django.db.models import F
from rest_framework.generics import ListAPIView
from tickers.models import StockHistory
from tickers.serializers import TradeHistorySerializer, TradeHistoryAnalyticsSerializer, TradeHistoryDeltaSerializer
from tickers.views.mixins import FilterDate


class _ApiTradeHistoryView(ListAPIView):
    def get_queryset(self):
        stock = self.kwargs.get('stock')
        return StockHistory.objects.select_related(
            'stock'
        ).filter(
            stock=stock
        )


class ApiTradeHistoryView(_ApiTradeHistoryView):
    serializer_class = TradeHistorySerializer

    def get_queryset(self):
        return super().get_queryset().three_months()


class ApiAnalyticsTradeHistory(_ApiTradeHistoryView, FilterDate):
    serializer_class = TradeHistoryAnalyticsSerializer
    filter_model_date_by = 'trade_date'

    def get_queryset(self):
        filter_dates = self.get_filter_dates()
        queryset = super().get_queryset()
        return self.by_date_filtered_queryset(queryset, filter_dates).annotate(
            delta_close_open=F('last_price') - F('open_price'),
            delta_high_low=F('high_price') - F('low_price')
        )


class ApiDeltaTradeHistory(_ApiTradeHistoryView):
    serializer_class = TradeHistoryDeltaSerializer
    delta_type_values = ['open', 'last', 'low', 'high']

    def get_queryset(self):
        lookup_value = Decimal(self.request.GET.get('value', False))
        delta_field = self.request.GET.get('type', False)
        return super().get_queryset().delta_sort_by_value(lookup_value, delta_field)