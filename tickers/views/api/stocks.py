from rest_framework.generics import ListAPIView
from tickers.models import Stock
from tickers.serializers import StocksSerializer


class ApiStocksListView(ListAPIView):
    serializer_class = StocksSerializer

    def get_queryset(self):
        return Stock.objects.all()
