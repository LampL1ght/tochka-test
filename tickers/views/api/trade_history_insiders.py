from rest_framework.generics import ListAPIView
from tickers.models import StockInnerTradesHistory
from tickers.serializers import ApiTradeHistoryInsidersSerializer, ApiInnerUserTradeHistorySerializer


class _AbstractTradeHistoryInsiders(ListAPIView):
    model = StockInnerTradesHistory

    def get_queryset(self):
        stock = self.kwargs.get('stock')
        return self.model.objects.select_related(
            'stock', 'transaction_type', 'insider'
        ).filter(
            stock=stock
        )


class ApiTradeHistoryInsidersListView(_AbstractTradeHistoryInsiders):
    serializer_class = ApiTradeHistoryInsidersSerializer

    def get_serializer_context(self):
        return {
            "stock": self.kwargs.get('stock'),
            "request": self.request
        }


class ApiInnerEmployeeTradeList(_AbstractTradeHistoryInsiders):
    serializer_class = ApiInnerUserTradeHistorySerializer

    def get_queryset(self):
        employee_name = self.kwargs.get('employee')
        return super().get_queryset().filter(insider__slug_name=employee_name)
