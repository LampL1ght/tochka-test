from django.views.generic import ListView
from tickers.models import StockInnerTradesHistory


class _AbstractTradeHistoryInsiders(ListView):
    context_object_name = 'InnerTrade'
    model = StockInnerTradesHistory

    def get_queryset(self):
        stock = self.kwargs.get('stock')
        return self.model.objects.select_related(
            'stock', 'transaction_type'
        ).filter(
            stock=stock
        )


class TradeHistoryInsidersListView(_AbstractTradeHistoryInsiders):
    template_name = 'tickers/stock_inner_trade_history_list.html'


class InnerEmployeeTradeList(_AbstractTradeHistoryInsiders):
    template_name = 'tickers/inner_employee_trade_history_list.html'

    def get_queryset(self):
        employee_name = self.kwargs.get('employee')
        return super().get_queryset().filter(insider__slug_name__iexact=employee_name)
