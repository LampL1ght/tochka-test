from django.views.generic import ListView
from tickers.models import Stock


class StocksListView(ListView):
    context_object_name = 'Stocks'
    model = Stock
