from django.db.models import F
from django.views.generic import ListView
from decimal import Decimal
from tickers.models import StockHistory
from tickers.views.mixins import FilterDate
from tickers.exceptions import NumberFound


class _TradeHistoryList(ListView):
    """
    Список стоков в таблице
    """
    context_object_name = 'TradeHistory'
    model = StockHistory

    def get_queryset(self):
        stock = self.kwargs.get('stock')
        return self.model.objects.select_related(
            'stock'
        ).filter(
            stock=stock
        )


class TradeHistoryListView(_TradeHistoryList):
    """
    История трейдов за 3 месяца
    """
    def get_queryset(self):
        return super().get_queryset().three_months()


class TradeHistoryAnalyticsListView(_TradeHistoryList, FilterDate):
    """
    Аналитика разниц цен по датам
    """
    template_name = 'tickers/trade_history_analytics.html'
    filter_model_date_by = 'trade_date'

    def get_queryset(self):
        filter_dates = self.get_filter_dates()
        queryset = super().get_queryset()
        return self.by_date_filtered_queryset(queryset, filter_dates).annotate(
            delta_close_open=F('last_price') - F('open_price'),
            delta_high_low=F('high_price') - F('low_price')
        )


class TradeHistoryDeltaListView(_TradeHistoryList):
    """
    Нахождение разницы минимальных цен по одному
    """
    template_name = 'tickers/trade_history_delta.html'
    delta_type_values = ['open', 'last', 'low', 'high']

    def perform_delta_sort(self):
        lookup_value = Decimal(self.request.GET.get('value', False))
        delta_field = self.request.GET.get('type', False)
        output = super().get_queryset().delta_sort_by_value(lookup_value, delta_field)
        return output

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['TradeHistory'] = self.perform_delta_sort()
        context['sort_values'] = self.delta_type_values
        return context
