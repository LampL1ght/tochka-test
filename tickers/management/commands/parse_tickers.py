from django.core.management.base import BaseCommand
from tickers.parser.main import Parser


class Command(BaseCommand):
    parser_types = [Parser.PARSE_HISTORICAL_DATA, Parser.PARSE_INNER_TRADES]
    help = f'Command to parse data about stocks from tickers.txt available parser commands: {parser_types}'

    def add_arguments(self, parser):
        parser.add_argument('number_of_threads', nargs='+', type=int, default=1)
        parser.add_argument('parser_type', nargs='+', type=str, default=Parser.PARSE_HISTORICAL_DATA)

    def handle(self, *args, **options):
        threads = int(options['number_of_threads'][0])
        parser_type = options['parser_type'][0]
        parser = Parser(threads, parser_type)
        parser.start()



