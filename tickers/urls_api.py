from django.urls import path
from tickers.views import api as api_view

urlpatterns = [
    path('', api_view.ApiStocksListView.as_view(), name='api-stocks-list'),
    path('<slug:stock>', api_view.ApiTradeHistoryView.as_view(), name='api-trade-history'),
    path('<slug:stock>/analytics', api_view.ApiAnalyticsTradeHistory.as_view(), name='api-analytics-trade-history'),
    path('<slug:stock>/delta', api_view.ApiDeltaTradeHistory.as_view(), name='api-delta-trade-history'),
    path('<slug:stock>/insider', api_view.ApiTradeHistoryInsidersListView.as_view(), name='api-trade-history-insiders'),
    path(
        '<slug:stock>/insider/<slug:employee>',
        api_view.ApiInnerEmployeeTradeList.as_view(),
        name='api-employee-trade-history'
    ),
]
