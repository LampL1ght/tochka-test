from rest_framework import serializers
from tickers.models import StockHistory


class TradeHistorySerializer(serializers.ModelSerializer):
    trade_date = serializers.DateField(format='%d/%m/%Y')

    class Meta:
        model = StockHistory
        fields = "__all__"


class TradeHistoryAnalyticsSerializer(TradeHistorySerializer):
    delta_close_open = serializers.DecimalField(max_digits=10, decimal_places=2, coerce_to_string=False)
    delta_high_low = serializers.DecimalField(max_digits=10, decimal_places=2, coerce_to_string=False)


class TradeHistoryDeltaSerializer(serializers.Serializer):
    obj_from = TradeHistorySerializer()
    obj_to = TradeHistorySerializer()
    price_change = serializers.DecimalField(max_digits=10, decimal_places=2, coerce_to_string=False)

