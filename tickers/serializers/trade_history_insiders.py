from rest_framework import serializers
from tickers.models import StockInnerTradesHistory, Employee
from django.urls import reverse


class InsiderSerializer(serializers.ModelSerializer):
    employee_statistics = serializers.SerializerMethodField()

    def get_employee_statistics(self, obj):
        stock = self.context.get('stock')
        request = self.context.get('request')
        return request.build_absolute_uri(
            reverse('api-employee-trade-history', kwargs={"stock": stock, "employee": obj.slug_name})
        )

    class Meta:
        model = Employee
        fields = "__all__"


class ApiInnerUserTradeHistorySerializer(serializers.ModelSerializer):
    last_price = serializers.DecimalField(max_digits=10, decimal_places=2, coerce_to_string=False)

    class Meta:
        model = StockInnerTradesHistory
        fields = "__all__"


class ApiTradeHistoryInsidersSerializer(ApiInnerUserTradeHistorySerializer):
    insider = InsiderSerializer()


