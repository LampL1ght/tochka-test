from rest_framework import serializers
from tickers.models import Stock


class StocksSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Stock
