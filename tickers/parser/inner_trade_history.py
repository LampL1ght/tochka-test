from django.db import IntegrityError, transaction
from tickers.parser.utils import PrepareModelData, ParseHtmlTable, ConvertDate, AbstractSaveData
from tickers.models import StockInnerTradesHistory, Employee, EmployeeRelation, TransactionType


class CreateInnerHistoryObject(PrepareModelData):
    def __init__(self, stock, data_array):
        self.stock_obj = self._get_stock_obj(stock)
        self.data_array = data_array
        self.bulk_create = []
        self._prepare_objects_data()

    model = StockInnerTradesHistory
    model_date_name = 'trade_date'

    @staticmethod
    def check_or_create_relation(relation):
        obj, created = EmployeeRelation.objects.get_or_create(
            name=relation, defaults={"name": relation}
        )
        return obj

    @staticmethod
    def check_or_create_transaction_type(data):
        name = data.get('transaction_type')
        obj, created = TransactionType.objects.get_or_create(
            name=name, defaults={"name": name}
        )
        return obj

    def check_or_create_inner_employee(self, data):
        lookup = {
            'full_name': data.get('insider'),
            'relation': self.check_or_create_relation(data.pop('relation')),
        }
        obj, created = Employee.objects.get_or_create(**lookup, defaults=lookup)
        return obj

    def _update_obj_data(self, data):

        data["stock"] = self.stock_obj
        data[self.model_date_name] = ConvertDate(data.pop(self.model_date_name)).get_date()
        data['insider'] = self.check_or_create_inner_employee(data)
        data['transaction_type'] = self.check_or_create_transaction_type(data)

        # Иногда цена закрытия может быть не указано. Сохраняем как None
        if data['last_price'] == '':
            data['last_price'] = None

        self.bulk_create.append(
            self.model(**data)
        )


class ProcessInnerTradesData(ParseHtmlTable):
    def __init__(self, stock):
        self.stock = stock.strip('\n').lower()
        self.raw_data = []
        self.link = f"http://www.nasdaq.com/symbol/{self.stock}/insider-trades"

    pages_number = 0
    table_parent_element_attr_value = 'genTable'
    table_parent_element_attr = 'class'

    table_headers = [
        'insider', 'relation', 'trade_date', 'transaction_type',
        'owner_type', 'shares_traded', 'last_price', 'shares_held'
    ]

    def get_extra_pages_number(self, bs_data):
        pages_elems = bs_data.find_all('a', {'class': 'pagerlink'})

        #  Если элементов больше 10, то берем 10
        if len(pages_elems) >= 10:
            self.pages_number = 10

        #  Если меньше, то сколько есть
        else:
            pages_len = 0
            for link in pages_elems:
                try:
                    link_text = int(link.text.strip())
                    pages_len = link_text
                #  Ошибка при попытке конвертировать next или last
                except ValueError:
                    pass
            self.pages_number = pages_len

    def get_table_parent_element(self):
        # переписываем этот метод т.к. нам нужно получить номера страниц для дальнейшего поиска
        parsed_data = self.bs_parsed_data()
        self.get_extra_pages_number(parsed_data)
        return parsed_data.find('div', {self.table_parent_element_attr: self.table_parent_element_attr_value})

    def get_table_rows(self):
        #  У этих ребят молодцов на этой странице нет открывающего тега tbody. Пришлось изменить метод
        table_parent = self.get_table_parent_element()
        return table_parent.find_all('tr')

    def iterate_table_rows(self):
        for tr in self.get_table_rows():
            values = []

            for td in tr.find_all('td'):
                values.append(self.try_convert_values(td))

            # если в tr тэге не было td элементов, то получим пустой лист.
            # Для него не стоит создавать объект
            if values:
                self.zip_create_object(values)

    def start_additional_pages(self):
        # Начинаем со второй странички т.к. первую уже спарсили
        original_link = self.link
        for page in range(2, self.pages_number):
            # просто подставляем get парамер к изначальной страничке
            self.link = f"{original_link}?page={page}"
            self.iterate_table_rows()

    def start(self):
        print(f'Started iteration for {self.stock}')
        try:
            self.iterate_table_rows()
            self.start_additional_pages()
            processed_data = CreateInnerHistoryObject(self.stock, self.raw_data)
            return processed_data.bulk_create, processed_data.stock_obj
        except AttributeError:
            print(f'Sees like for stock with name {self.stock} something went wrong')
            print(f'It may be due security reasons')


class SaveInnerTrades(AbstractSaveData):
    def __init__(self, bulk_create_data, stock_obj):
        self.bulk_create_data = bulk_create_data
        self.stock_obj = stock_obj
        self.bulk_create()

    def get_last_created_obj(self):
        try:
            return StockInnerTradesHistory.objects.filter(
                stock=self.stock_obj
            ).only(
                'trade_date'
            ).latest(
                'trade_date'
            )
        except StockInnerTradesHistory.DoesNotExist:
            return None

    def should_object_create(self, creating_obj, last_obj):
        # Проверить существуют ли предыдущие объекты
        if last_obj:
            if creating_obj.trade_date > last_obj.trade_date:
                return True
            else:
                return False
        else:
            return True

    def bulk_create(self):
        last_obj = self.get_last_created_obj()
        for obj in self.bulk_create_data:
            if self.should_object_create(obj, last_obj):
                # Похоже, что объект уже в базе. Ничего не делаем если не проходит валидация на уникальность
                try:
                    with transaction.atomic():
                        obj.save()
                except IntegrityError:
                    pass

    model = StockInnerTradesHistory
