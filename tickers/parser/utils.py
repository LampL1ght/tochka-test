from bs4 import BeautifulSoup
from decimal import Decimal
from decimal import InvalidOperation
from datetime import date
from django.db import IntegrityError, transaction
from tickers.models import Stock
from tickers.parser.exceptions import StockNotFound, CantReachRemoteServer
from requests import get


class ConvertDate:
    def __init__(self, date):
        self.splited_date = date.split('/')

    def _date_is_full(self):
        if len(self.splited_date) == 3:
            return True
        else:
            return False

    def get_date(self):
        if self._date_is_full():
            month, day, year = self.splited_date
            return date(year=int(year), month=int(month), day=int(day))
        else:
            return date.today()


class AbstractSaveData:
    model = None
    bulk_create_data = []
    stock_obj = None

    def get_model(self):
        return self.model

    def bulk_create(self):
        for obj in self.bulk_create_data:
            try:
                with transaction.atomic():
                    obj.save()
            # Похоже, что объект уже в базе. Ничего не делаем если не проходит валидация на уникальность
            except IntegrityError:
                pass


class PrepareModelData:
    bulk_create = []
    data_array = []
    stock_obj = None
    model_date_name = ''
    model = None

    @staticmethod
    def _get_stock_obj(stock):
        stock, created = Stock.objects.get_or_create(
            name=stock, defaults={"name": stock}
        )
        return stock

    def _update_obj_data(self, data):
        data["stock"] = self.stock_obj
        data[self.model_date_name] = ConvertDate(data.pop(self.model_date_name)).get_date()

        self.bulk_create.append(
            self.model(**data)
        )

    def _prepare_objects_data(self):
        for data_obj in self.data_array:
            self._update_obj_data(data_obj)


class ParseHtmlTable:
    SOUP_PARSER = 'lxml'
    link = ""
    table_headers = []
    raw_data = []
    stock = None

    table_parent_element_attr_value = ''
    table_parent_element_attr = ''

    def get_url_content(self):
        response = get(self.link)
        if response.status_code == 200:
            return response.content
        elif response.status_code == 404:
            raise CantReachRemoteServer("Cannot reach remote server. Try again later.")
        else:
            raise StockNotFound(f'Stock {self.stock} is not found.')

    def bs_parsed_data(self):
        return BeautifulSoup(self.get_url_content(), self.SOUP_PARSER)

    def get_table_parent_element(self):
        return self.bs_parsed_data().find(
            'div', {self.table_parent_element_attr: self.table_parent_element_attr_value}
        )

    def get_table_rows(self):
        table_parent = self.get_table_parent_element()
        if self.table_parent_element_attr == 'class':
            table_parent = table_parent
        return table_parent.find('tbody').find_all('tr')

    def zip_create_object(self, values):
        if values:
            data = dict(zip(self.table_headers, values))
            self.raw_data.append(data)

    def iterate_table_rows(self):
        for tr in self.get_table_rows():
            values = []
            for td in tr.find_all('td'):
                if td.text.strip():
                    values.append(self.try_convert_values(td))
            if values:
                self.zip_create_object(values)

    @staticmethod
    def try_convert_values(value):
        value = value.text.strip()

        if value == 'N/A':
            value = 0

        if value:

            try:
                return Decimal(value.replace(',', ''))
            except InvalidOperation:
                pass

            try:
                return int(value.replace(',', ''))
            except ValueError:
                pass

        return value
