from tickers.parser.utils import PrepareModelData, ParseHtmlTable, AbstractSaveData
from tickers.models import StockHistory


class PrepareHistoricalData(PrepareModelData):
    def __init__(self, stock, data_array):
        self.stock_obj = self._get_stock_obj(stock)
        self.data_array = data_array
        self._prepare_objects_data()

    model_date_name = 'trade_date'
    model = StockHistory


class ProcessHistoricalData(ParseHtmlTable):
    def __init__(self, stock):
        self.stock = stock.strip('\n').lower()
        self.raw_data = []
        self.link = f"http://www.nasdaq.com/symbol/{self.stock}/historical"

    table_parent_element_attr_value = 'genTable'
    table_parent_element_attr = 'class'

    table_headers = [
        'trade_date', 'open_price', 'high_price', 'low_price', 'last_price', 'volume'
    ]

    def start(self):
        print(f'Started iteration for {self.stock}')
        self.iterate_table_rows()
        processed_data = PrepareHistoricalData(self.stock, self.raw_data)
        return processed_data.bulk_create, processed_data.stock_obj


class SaveHistoricalData(AbstractSaveData):
    def __init__(self, bulk_create_data, stock_obj):
        self.bulk_create_data = bulk_create_data
        self.stock_obj = stock_obj
        self.bulk_create()

    model = StockHistory
