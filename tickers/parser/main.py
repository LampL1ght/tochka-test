from multiprocessing import Pool
from tickers.parser.trade_history import ProcessHistoricalData, SaveHistoricalData
from tickers.parser.inner_trade_history import ProcessInnerTradesData, SaveInnerTrades


def get_historical_data(stock):
    data = ProcessHistoricalData(stock)
    return data.start()


def get_inner_trades_data(stock):
    data = ProcessInnerTradesData(stock)
    return data.start()


class Parser:
    PARSE_HISTORICAL_DATA = 'trades_history'
    PARSE_INNER_TRADES = 'inner_trades'

    def __init__(self, threads, parser_type):
        self.stocks = open('./tickers.txt', 'r').readlines()
        self.parser_type = parser_type
        self.threads = threads

    def start(self):
        with Pool(self.threads) as p:
            if self.parser_type == self.PARSE_HISTORICAL_DATA:
                output = p.map(get_historical_data, self.stocks)
                for prepared_data_and_stock in output:
                    if prepared_data_and_stock:
                        data, stock = prepared_data_and_stock
                        SaveHistoricalData(data, stock)
                p.close()
                p.join()
            elif self.parser_type == self.PARSE_INNER_TRADES:
                output = p.map(get_inner_trades_data, self.stocks)
                for prepared_data_and_stock in output:
                    if prepared_data_and_stock:
                        data, stock = prepared_data_and_stock
                        SaveInnerTrades(data, stock)

                p.close()
                p.join()
