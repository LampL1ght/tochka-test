class StockNotFound(Exception):
    pass


class CantReachRemoteServer(Exception):
    pass
