from django.urls import path
from tickers import views

urlpatterns = [
    path('', views.StocksListView.as_view(), name='stocks-list'),
    path('<slug:stock>', views.TradeHistoryListView.as_view(), name='trade-history'),
    path('<slug:stock>/insider', views.TradeHistoryInsidersListView.as_view(), name='trade-history-insiders'),
    path(
        '<slug:stock>/insider/<slug:employee>',
        views.InnerEmployeeTradeList.as_view(),
        name='employee-trade-history'
    ),
    path('<slug:stock>/analytics', views.TradeHistoryAnalyticsListView.as_view(), name='trade-history-analytics'),
    path('<slug:stock>/delta', views.TradeHistoryDeltaListView.as_view(), name='trade-history-delta')
]
